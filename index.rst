﻿.. |label| replace:: Konfigurierbare Attribut-Textbausteine
.. |snippet| replace:: FvCustomFields
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.1
.. |maxVersion| replace:: 5.4.6
.. |version| replace:: 1.0.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Plugin "Konfiguriebare Attribut-Textbausteine" erweitert die Artikeldetailseite um einen Textbaustein, der je nach Inhalt eines bestimmten Artikel-Attributs erscheint.


Frontend
--------

.. image:: FvCustomFieldsFrontend.png

Auf der Artikeldetailseite erscheint zwischen der Angabe der Mehrwertsteuer und der Verfügbarkeit/Lieferzeit je nach Inhalt des angewählten Artikelattributs (s. Backend) ein zusätzlicher Textbaustein, der zunächst den Text "custom field 1" enthält.

Backend
-------

.. image:: FvCustomFieldsBackend.png

Im Backend wird eines der Felder aus der Tabelle s_articles_attributes ausgewählt. Wenn der Eintrag in diesem Feld zu dem zugehörigen Artikel "1" ist, so erscheint der Textbaustein auf der Artikeldetailseite.




technische Beschreibung
------------------------



Modifizierte Template-Dateien
-----------------------------
:/detail/data.tpl:




